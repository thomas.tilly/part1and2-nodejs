const express = require('express');
const app = express()
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }))


app.get('/', function (request, response) {
    response.send('Hello World');
});

app.get('/hello', function (request, response) {
    if (Object.entries(request.query).length === 0) {
        response.send('Quel est votre nom ?')
    } else {
        response.send('Bonjour ' + request.query.nom + ' !')
    }
    response.send('Quel est votre nom ?');
});

app.post('/chat', function (request, response) {
    console.log(request.body)
    if (request.body.msg === 'ville') {
        response.json('Nous sommes à Paris')
    } else if (request.body.msg === 'météo') {
        response.json('Il fait beau')
    }
});

app.listen(3000, function () {
    console.log("Server running at http://localhost:3000");
});

