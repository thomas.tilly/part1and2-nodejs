# Part1

Exercice 1 : <br>

Le programme que l'on vient de créer permet de lancer un serveur sur le LocalHost:3000. Le serveur va afficher Hello World grâce à la requête GET que nous avons fait en recharchant la page du navigateur.<br>
Pour installer le programme il suffit simplement de cloner le dossier git avec le serveur, puis faire npm install express --save (va permettre à créer un serveur web), et ensuite executer la commande curl http://localhost:3000/ ou la commande nodejs server.js.<br>
Normalement celà devrait vous afficher à l'adresse http://localhost:3000/ une ligne avec écrit Hello World.

